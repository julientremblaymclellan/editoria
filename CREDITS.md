<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Editoria 1.0, July 2017](#editoria-10-july-2017)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

### Editoria 1.0, July 2017  
Editoria was started as a collaboration between UCP and Coko, initial funding came from the Mellon Foundation.  

Those involved in conceptualising, designing, and building Editoria (including the Wax Editor) from 0 to 1.0 include:  

* Erich van Rijn  
* Catherine Mitchell  
* Kate Warne  
* Cindy Fulton  
* Lisa Schiff  
* Justin Gonder  
* Juliana Froggatt  
* Adam Hyde  
* Kristen Ratan  
* Yannis Barlas  
* Christos Kokosias  
* Julien Taquet  
* Alex Theg  
* Alexandros Georgantas  
* Giannis Kopanas  
