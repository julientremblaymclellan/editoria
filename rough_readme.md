# Editoria

Collaborative Book production platform

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. See deployment for notes on how to deploy the project on a live system.

### Prerequisites

### Check for all required packages
```sh
curl --version; git --version ;echo "node version =" && node --version;echo "npm version=" && npm --version;echo "yarn version=" && yarn --version; echo "docker version=" &&docker --version; echo "docker-compose version=" && docker-compose --version;

```

|     Package     	| Compatible Versions 	|       Verify version      	|             Install            	|
|:---------------:	|:-------------------:	|:-------------------------:	|:------------------------------:	|
|       node      	|        8.3 ⬆️        	|       node --version      	| sudo apt-get install -y nodejs 	|
|       nvm       	|         any         	|       nvm --version       	| curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.34.0/install.sh | bash                               	|
|       npm       	|         any         	|       npm --version       	|   sudo apt-get install -y npm  	|
|       yarn      	|       1.3.2 ⬆️       	|       yarn --version      	|      sudo  npm install -g yarn      	|
|      docker     	|       18.X.X ⬆️      	|      docker --version     	|       See [Docker documentation](https://docs.docker.com/install/)  -   [:exclamation::warning: Complete Linux post-install ](https://docs.docker.com/install/linux/linux-postinstall/)     	|
| docker-composer 	|        1.2.XX       	| docker-composer --version 	|         See [Docker documentation](https://docs.docker.com/compose/install/) -          	|https://docs.docker.com/compose/install/


## Installing

A step by step series of examples that tell you how to get a development env running

Say what the step will be

```
Give the example
```

And repeat

```
until finished
```

End with an example of getting some data out of the system or using it for a little demo

## Running the tests

Explain how to run the automated tests for this system

### Break down into end to end tests

Explain what these tests test and why

```
Give an example
```

### And coding style tests

Explain what these tests test and why

```
Give an example
```

## Deployment

Add additional notes about how to deploy this on a live system

## Built With

* [Dropwizard](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [Maven](https://maven.apache.org/) - Dependency Management
* [ROME](https://rometools.github.io/rome/) - Used to generate RSS Feeds

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/your/project/tags). 

## Authors

* **Billie Thompson** - *Initial work* - [PurpleBooth](https://github.com/PurpleBooth)

See also the list of [contributors](https://github.com/your/project/contributors) who participated in this project.

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Hat tip to anyone whose code was used
* Inspiration
* etc

> Written with [StackEdit](https://stackedit.io/).
