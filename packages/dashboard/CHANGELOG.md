<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Change Log](#change-log)
  - [0.1.4 (2018-11-26)](#014-2018-11-26)
    - [Bug Fixes](#bug-fixes)
  - [0.1.3 (2018-11-20)](#013-2018-11-20)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.1.4"></a>
## [0.1.4](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.1.3...pubsweet-component-editoria-dashboard@0.1.4) (2018-11-26)


### Bug Fixes

* use of the correct function's name ([82d2701](https://gitlab.coko.foundation/editoria/editoria-dashboard/commit/82d2701))




<a name="0.1.3"></a>
## [0.1.3](https://gitlab.coko.foundation/editoria/editoria-dashboard/compare/pubsweet-component-editoria-dashboard@0.1.2...pubsweet-component-editoria-dashboard@0.1.3) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-editoria-dashboard
