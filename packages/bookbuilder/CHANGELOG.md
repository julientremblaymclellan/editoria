<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Change Log](#change-log)
  - [1.1.5 (2018-11-20)](#115-2018-11-20)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="1.1.5"></a>
## [1.1.5](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-bookbuilder@1.1.4...pubsweet-component-bookbuilder@1.1.5) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-bookbuilder
