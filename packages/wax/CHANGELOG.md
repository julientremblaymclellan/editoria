<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**  *generated with [DocToc](https://github.com/thlorenz/doctoc)*

- [Change Log](#change-log)
- [0.7.0 (2018-12-19)](#070-2018-12-19)
    - [Features](#features)
  - [0.6.16 (2018-11-20)](#0616-2018-11-20)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

<a name="0.7.0"></a>
# [0.7.0](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.6.16...pubsweet-component-wax@0.7.0) (2018-12-19)


### Features

* upgraded substance ([466f92a](https://gitlab.coko.foundation/editoria/editoria/commit/466f92a))




<a name="0.6.16"></a>
## [0.6.16](https://gitlab.coko.foundation/editoria/editoria/compare/pubsweet-component-wax@0.6.15...pubsweet-component-wax@0.6.16) (2018-11-20)




**Note:** Version bump only for package pubsweet-component-wax
